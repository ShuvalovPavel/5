#include "stack.h"
using namespace std;

Stack::Stack(int sz) 
{
	ptrstack=new string[sz];
	ts=-1;
}

Stack::Stack(const Stack &otherStack)
{ 
	ts=otherStack.ts;
	sz=otherStack.sz;
	ptrstack=new string[sz]; 
	for(int j = 0; j < ts; j++)
		ptrstack[j] = otherStack.ptrstack[j];
}

Stack::~Stack() 
{
	delete []ptrstack;
}

std::istream& operator >>(std::istream& in, Stack& r)
{
	string st;
    cout << "ведите строку: "<<endl;
    in >> st;
    r.push(st);
	return in;
}

std::ostream& operator <<(std::ostream& out, const Stack& r)
{
	for (int i=0; i<=r.ts; i++)
        out << r.ptrstack[i] << endl;
	return out;
}

void Stack::push(string itm) 
{
	if (ts>=sz)
	{
		cout<<"стак переполнен"<<endl;
	}
	else 
		ptrstack[ts++] = itm;
}

string Stack::pop() 
 {
 	if (ts < 0)
 	{
 		cout<<"стак пуст"<<endl;
		return ptrstack[ts];
 	} 
 	else 
		return ptrstack[ts--];
 }