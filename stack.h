#pragma once

#include <iostream>
using namespace std;
class Stack 
{
private:
	string *ptrstack;
	int ts;
	int sz;

public:
	Stack(int sz); 
	Stack(const Stack &otherStack);
	~Stack(); 
	friend std::istream& operator >>(std::istream& in, Stack& r);
    friend std::ostream& operator <<(std::ostream& out, const Stack& r);
	void push(string itm); 
	string pop(); 
};